/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_VERSIONS_SURFACEBACKENDCONTAINER_V1_H
#define XAODTRACKING_VERSIONS_SURFACEBACKENDCONTAINER_V1_H

#include "AthContainers/DataVector.h"
#include "xAODTracking/versions/SurfaceBackend_v1.h"

namespace xAOD {
    typedef DataVector<xAOD::SurfaceBackend_v1> SurfaceBackendContainer_v1;
}


#endif